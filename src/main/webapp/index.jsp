<html>
<head>
	<title>API Specification Validator</title>
</head>
<body>
	<h2>API Specification Validator</h2>
	<form action="validate.jsp" method="POST" enctype="multipart/form-data" >
	
		Type of API file to validate:<br>
		<input type="radio" name="type" value="raml08" checked />RAML 0.8<br>
		<input type="radio" name="type" value="raml10" />RAML 1.0<br>
		<input type="radio" name="type" value="swagger" />Swagger<br>
		<input type="radio" name="type" value="blueprint" />BluePrint<br>
		<br>
		File: <input type="file" name="filename" /><br>
		<br>
		<input type="submit" value="Submit" />
	
	</form>
</body>
</html>
