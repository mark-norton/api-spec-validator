<%@ page import = "java.io.*,java.util.*, javax.servlet.*" %>
<%@ page import = "javax.servlet.http.*" %>
<%@ page import = "org.apache.commons.fileupload.*" %>
<%@ page import = "org.apache.commons.fileupload.disk.*" %>
<%@ page import = "org.apache.commons.fileupload.servlet.*" %>
<%@ page import = "org.apache.commons.io.output.*" %>
<%@ page import="com.ms3_inc.api.ApiValidate" %>
<%  
	ApiValidate handler = new ApiValidate();
%>
<html>
<head>
	<title>API Specification Validator - Results</title>
</head>
<body>
	<h2>API Specification Validator - Results</h2>
	
	<%
	   //  The file to validate.
	   File file;
	   
	   //	Some size limits to prevent upload problems.
	   int maxFileSize = 5000 * 1024;
	   int maxMemSize = 5000 * 1024;
	   
	   //	Attributes and file are accessed via the servlet context.
	   ServletContext context = pageContext.getServletContext();
	   String filePath = context.getInitParameter("file-upload");	   
	
	   // Verify the content type
	   String contentType = request.getContentType();
	   
	   if ((contentType.indexOf("multipart/form-data") >= 0)) {
	      DiskFileItemFactory factory = new DiskFileItemFactory();
	      
	      // maximum size that will be stored in memory
	      factory.setSizeThreshold(maxMemSize);
	      
	      // Location to save data that is larger than maxMemSize.
	      //  factory.setRepository(new File("c:\\temp"));
	
	      // Create a new file upload handler
	      ServletFileUpload upload = new ServletFileUpload(factory);
	      
	      // maximum file size to be uploaded.
	      upload.setSizeMax( maxFileSize );
	      
	      try { 
	         // Parse the request to get file items.
	         List fileItems = upload.parseRequest(request);
	
	         // Process the uploaded file items
	         Iterator i = fileItems.iterator();
	         
	         //	Collected parameters.
	         String fieldName = null;
	         String fileName = null;
	         String typeName = null;
	         String typeValue = null;
	         InputStream in = null;
	
			 // Iterate over all items in the multi-part form.
	         while ( i.hasNext () ) {
	            FileItem fi = (FileItem)i.next();
	            
	            if ( !fi.isFormField () ) {
	               // Get the uploaded file parameters
	               fieldName = fi.getFieldName();
	               fileName = fi.getName();
	               
	               boolean isInMemory = fi.isInMemory();
	               long sizeInBytes = fi.getSize();
	               
	               in = fi.getInputStream();
	            }
	            else {
	            	typeName = fi.getFieldName();
	            	if (typeName.compareTo("type")==0) {
	            		typeValue = fi.getString();
	            	}
	            }
	         }

			 //	 Validation.
			 if (typeValue == null)
			     typeValue = "unknown";
	         out.println(handler.validate(in, fileName, typeValue));

	      } 
	      catch(Exception ex) {
	          out.println("<p>Error encountered: "+ex.getMessage()+"</p>");
	      }
	   } 
	   else {
	      out.println("<p>No file uploaded</p>");
	   }
	%>	
	<br>
	<a href="index.jsp">Validate Another File</a><br>
</body>
</html>
