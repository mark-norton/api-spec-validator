package com.ms3_inc.api;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Vector;


/**
 * The MarkDownTest application parses a file formatted using the MarkDown language and validates it against rules drawn from the
 * <a href="https://github.com/apiaryio/api-blueprint/blob/master/API%20Blueprint%20Specification.md">API BluePrint specification</a>.
 *  
 * @author Mark J. Norton - mnorton@ms3-inc.com
 *
 */
public class MarkDownTest {
	public static String[] COLLAPSE = {"model", "schema", "request", "response", "parameters", "attributes", "headers", "body"};
	public static String[] GOLD_EX = {
		"resources/bp/01-simplest-api.md",
		"resources/bp/02-resources-actions.md",
		"resources/bp/03-named-resources-actions.md",
		"resources/bp/04-grouping-resources.md",
		"resources/bp/05-responses.md",
		"resources/bp/06-requests.md",
		"resources/bp/07-parameters.md",
		"resources/bp/08-attributes.md",
		"resources/bp/09-advanced-attributes.md",
		"resources/bp/10-data-structures.md",
		"resources/bp/11-resource-model.md",
		"resources/bp/12-advanced-action.md",
		"resources/bp/13-named-endpoints.md",
		"resources/bp/14-json-schema.md",
		"resources/bp/15-adv-json-schema.md"
	};
	
	public static MarkDownTest app = new MarkDownTest();
	public static boolean valid = true;						//	Overall validation flag.
	public static boolean treePrint = false;				//	Print full tree form.
	public static boolean debug = false;					//	Set to true for tracing.

	/**
	 * Main entry point.
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		MarkDownTest.app.validateGoldExamples();
		//MarkDownTest.app.validateOneOffExample();
	}
	
	/**
	 * Validate all Gold Standard examples for API BluePrint.
	 */
	public void validateGoldExamples() {
		for (String fn : MarkDownTest.GOLD_EX) {
			try {
				this.validate(fn);
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	/**
	 * Validate all API Pro BluePrint examples.
	 * 
	 */
	public void validateApiProExamples() {
		try {
			this.validate("resources/bp/datatypes-blueprint.md");
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Validate a specific file.  Useful for debugging, etc.
	 * 
	 */
	public void validateOneOffExample() {
		try {
			this.validate("resources/bp/05-responses.md");
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	
	/**
	 * Validate an API BluePrint file.
	 * 
	 * NOTE:  This approach has a fairly large number of passes for a parse/validator.  It could be made more efficient to merge some of these passes,
	 * but breaking them out into separate passes makes the algorithms easier to understand.  Efficiency would only be impacted by an extremely large
	 * API specification, but even then the difference might not be noticed.
	 * 
	 * @throws Exception
	 */
	public void validate(String fileName) throws Exception {
		if (fileName == null) {
			System.out.println("*** FAIL ***  File name to validate is null.");
			return;
		}
		
		System.out.println("\n===========================================================");
		System.out.println("Validating: "+fileName);
		
		BpNode root = new BpNode("ROOT");
		List<BpNode> nodeList = null;
		
		//	Pass 1:  parse lines and identify sections.
		// this.parseLines("resources/valid/datatypes-blueprint.md");	//	API Pro generated example.
		nodeList = this.parseLines_1(fileName);							//	Resources and Actions gold example.
		root.setChildren(nodeList);
		
		//	Pass 2:  Identify Metadata section lines.
		this.idMetadata_2(nodeList);
		
		//	Pass 3:  collapse embedded comments.
		nodeList = this.collapseComments_3(nodeList);
		root.setChildren(nodeList);
		
		//	Pass 4:  extract header information.
		this.extractHeaderInfo_4(nodeList);
		
		//	Pass 5:  extract list information.
		this.extractListInfo_5(nodeList);
		
		//	Pass 6:  collapse any list blocks.
		nodeList = this.collapseLists_6(nodeList);
		root.setChildren(nodeList);

		//	Print the node list to console.
		System.out.println("");
		if (MarkDownTest.treePrint)
			this.treePrint(0, root);			//	Prints the whole tree with indentations.
		else
			this.flatPrint(nodeList);			//	Prints just the top level sections.
	
	}

	
	/****************************************************
	 *   Parse and Validate Passes.                     *
	 ***************************************************/

	/**
	 * Pass 1:  
	 * Parse lines from the input document.  Identify blank lines, sections, and extract indentation.
	 * 
	 * @param fileName
	 * @return List of BpNode
	 * @throws Exception
	 */
	public List<BpNode> parseLines_1(String fileName) throws Exception {
		String line;

	    InputStream fis = new FileInputStream(fileName);
	    InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
	    BufferedReader br = new BufferedReader(isr);

	    List<BpNode> nodes = new Vector<BpNode>();
	    
	    //	Read all lines in the file, create nodes, and add them to a simple list.
	    while ((line = br.readLine()) != null) {
        	BpNode child = new BpNode(line);
        	nodes.add(child);
	    }
	    	    
	    return nodes;
	}
	
	
	/**
	 * Pass 2:  
	 * Identify metadata elmements.
	 * Checks for a FORMAT element.
	 * 
	 * @param nodeList
	 */
	public void idMetadata_2(List<BpNode> nodeList) {
		boolean formatFlag = false;											//	All documents must have a FORMAT metadata line.
		for (BpNode node : nodeList) {
			if (node.getType() == BpNode.Type.UNKNOWN) {
				node.setType(BpNode.Type.METADATA);
				
				//	See if we have a FORMAT line.
				if (node.getLine().indexOf("FORMAT:") != -1)
					formatFlag = true;
			}
			else
				break;															//	Stop at the first section.
		}
		
		//	Check for a FORMAT:  element.
		if (formatFlag == false) {
			MarkDownTest.valid = false;
			System.out.println("*** FAIL ***  Document does not contain a FORMAT metadata element.");
		}
		
	}
	
	/**
	 * Pass 3:  
	 * Collapse embedded comments.
	 * 
	 * Embedded comments always appear after a Header section and is terminated by a new section:  either a header or list.
	 * When a comment block is found, a new node is created of type DESC_BLOCK.  Subsequent embedded comments are added as
	 * children to that node.
	 * 
	 * @param nodeList
	 * @return modified node list.
	 */
	public List<BpNode> collapseComments_3(List<BpNode> nodeList) {
		List<BpNode> collapseList = new Vector<BpNode>();

		BpNode descBlock = null;			//	Section header we are collapsing into.
		boolean inComment = false;		//	State flag set at start of a comment block.
		
		//	Iterate over all nodes.
		for (BpNode node : nodeList) {
			BpNode.Type type = node.getType();
			
			//	In a comment block?
			if (inComment) {
				
				//	Check for a comment to collapse.  Lump blank lines into a comment.
				if (type == BpNode.Type.UNKNOWN || type == BpNode.Type.BLANK) {
					
					//	We are supposed to have a description block at this point, if not, we have a problem.
					if (descBlock == null) {
						MarkDownTest.valid = false;
						System.out.println("*** FAIL ***  Embedded description detected outside of a header section.");
					}
					
					//	Collapse the embedded comment element into the current description block.
					else {
						node.setType(BpNode.Type.DESC);
						descBlock.addChild(node);
					}
				}
				
				//	Check to see if the comment block is terminated by a List section.
				else if (type == BpNode.Type.LIST) {
					inComment = false;			//	Flag the end of an embedded comment block.
					descBlock = null;			//	No longer have a header to collapse into.
					collapseList.add(node);		//	Add the list node.
				}
				
				//	Check to see if the comment block is terminated by a new Header section.
				else if (type == BpNode.Type.HEAD) {
					inComment = true;			//	Flag the start of a new comment block.  This is redundant, but consistent.
					descBlock = new BpNode("DESCRIPTION-BLOCK");	//	Create a new description block node.
					descBlock.setType(BpNode.Type.DESC_BLOCK);		//	Set the description block type.
					node.addChild(descBlock);	//	Add the new description block to the header node.
					collapseList.add(node);		//	Add the header to the output list.				
				}
				
				//	Commented terminated by something other than a list or header.
				else {
					inComment = false;			//	Flag the end of the current comment block.
					descBlock = null;			//	No longer have a header to collapse into.
					collapseList.add(node);		//	What ever it is, add it to the output list.

					MarkDownTest.valid = false;
					System.out.println("*** FAIL ***  Embedded description terminated by something other than head or list: "+type.toString());
				}
			}
			
			//	Not in a comment block, check for a header start.
			else if (type == BpNode.Type.HEAD) {
				inComment = true;			//	Flag the start of an embedded comment block.
				descBlock = new BpNode("DESCRIPTION-BLOCK");	//	Create a new description block node.
				descBlock.setType(BpNode.Type.DESC_BLOCK);		//	Set the description block type.
				node.addChild(descBlock);	//	Add the new description block to the header node.
				collapseList.add(node);		//	Add the header to the output list.
			}
			
			//	Not in a comment block, just copy the element.
			else {
				collapseList.add(node);		//	Could be a model, properties, or any number of things.
			}
		}

		return collapseList;
	}
	
	/**
	 * Pass 4:
	 * Extract information from a header section.
	 * 
	 * Header section types include:
	 * <ul>
	 * <li>API Name - position in doc</li>
	 * <li>Resource Group - keyword</li>
	 * <li>Data Structures - keyword</li>
	 * <li>Resource - four patterns</li>
	 * <li>Action - three patterns</li>
	 * </ul>
	 * Information extracted may include:
	 * <ul>
	 * <li>Keywords:  Group, Data Structures</li>
	 * <li>Identifier:  string</li>
	 * <li>URI Template: starts with a slash</li>
	 * <li>Method:  keywords</li>
	 * </ul>
	 * @param nodeList
	 */
	public void extractHeaderInfo_4(List<BpNode> nodeList) {
		//	Iterate over the top level of nodes.
		for (BpNode node : nodeList) {
			String line = node.getLine();
			
			//	We are only working with headers.
			if (node.getType() == BpNode.Type.HEAD) {
				String method = node.findMethod();

				//	Check for a Resource Group.			Note the space in search spec.
				if (line.indexOf("Group ") != -1) {
					node.setKeyword("Group");			//	Save the keyword.
					String id = line.substring("Group".length()).trim();	//	Extract the identifier.
					node.setIdentifier(id);
				}
				
				//	Check for a Data Structure
				else if (line.indexOf("Data Structures") != -1) {
					node.setKeyword("Data Structures");
				}

				//	Check for an API Name section.
				else if (node.getLevel() == 1 && method == null && line.indexOf('[') == -1 && line.indexOf('/') == -1) {
					String id = line.trim();			//	Extract the identifier, which is the API namne.
					node.setIdentifier(id);
					node.setKeyword("API Name");		//	This keyword is not in the spec.
				}

					//	Resource Section Pattern 1:  # <URI>
				else if (line.charAt(0) == '/') {
					node.setUri(line);
					node.setKeyword("Resource");		//	This keyword is not in the spec.
				}
				
				//	Resource Section Pattern 2:  # <identifier> [<URI>]
				else if (method == null && line.indexOf("[/") != -1) {
					String identifier = line.substring(0, line.indexOf("[/"));
					identifier.trim();
					node.setIdentifier(identifier);
					String uri = line.substring(line.indexOf('[')+1, line.indexOf(']'));
					node.setUri(uri);
					node.setKeyword("Resource");		//	This keyword is not in the spec.
				}
				
				//	Resource Section Pattern 3:  # <Request Method> <URI>
				else if (method != null && line.startsWith(method) && line.indexOf('/') != -1) {
					node.setMethod(method);
					String uri = line.substring(method.length()+1);
					uri.trim();
					node.setUri(uri);
					node.setKeyword("Resource");		//	This keyword is not in the spec.
				}
				
				//	Resource Section Pattern 4:  # <identifier> [<Request Method> <URI>]
				else if (method != null && line.indexOf("["+method) != -1 && line.indexOf('/') != -1) {
					String identifier = line.substring(0, line.indexOf('['));
					identifier.trim();
					node.setIdentifier(identifier);
					node.setMethod(method);
					String uri = line.substring(line.indexOf('/'), line.indexOf(']'));
					node.setUri(uri);
					node.setKeyword("Resource");		//	This keyword is not in the spec.		
				}
			
				//	Action Section Pattern 1:  ## <Request Method>
				else if (method != null && line.indexOf('[') == -1) {
					node.setMethod(method);
					node.setKeyword("Action");		//	This keyword is not in the spec.
				}
				
				else if (method != null && line.indexOf('[') != -1) {
					String identifier = line.substring(0, line.indexOf('['));
					identifier.trim();
					node.setIdentifier(identifier);
					node.setMethod(method);
					node.setKeyword("Action");		//	This keyword is not in the spec.
				}
				
				//	Action Section Pattern 3:  # <identifier> [<Request Method> <URI>]   -- identical pattern to Resource 4 above, thus this will never be reached!
				else if (method != null && line.indexOf("["+method) != -1) {
					String identifier = line.substring(0, line.indexOf('['));
					identifier.trim();
					node.setIdentifier(identifier);
					node.setMethod(method);
					String uri = line.substring(line.indexOf('/'), line.indexOf(']'));
					node.setUri(uri);
					node.setKeyword("Action");		//	This keyword is not in the spec.		
				}

				//	Section pattern is unknown.
				else {
					System.out.println("*** FAIL ***  Unknown header section pattern: "+line);
				}
			}
		}
		
	}
	
	/**
	 * Pass 5:
	 * Extract information from a list section.
	 * 
	 * List section types include:
	 * <ul>
	 * <li>Schema<li>
	 * <li>Parameters</li>
	 * <li>Headers<li>
	 * <li>Body</li>
	 * <li>Data Structures</li>
	 * <li>Model (<media type>)</li>
	 * <li>Request <identifier> (<media type>)</li>
	 * <li>Response <status code> (<media type>)</li>
	 * <li>Attributes <mson type definition></li>
	 * <li>Relation: <link relation id></li>
	 * </ul>
	 * All list sections have a keyword.
	 * 
	 * @param nodeList
	 */
	public void extractListInfo_5(List<BpNode> nodeList) {
		//	Iterate over the top level of nodes.
		for (BpNode node : nodeList) {
			String line = node.getLine();
			
			//	We are only working with lists.
			if (node.getType() == BpNode.Type.LIST) {
				String kw = node.findKeyword();
				if (kw == null) kw = "UNKNOWN";
				
				String remainder = line.substring(kw.length());	//	 line with keyword stripped off.
				
				//	Switch on keyword.
				switch (kw) {
					//	These cases just have a keyword.
					case "schema":
					case "parameters":
					case "headers":
					case "body":
					case "data structure":
						node.setKeyword(kw);
						break;
					
					//  These cases have a keyword, response code, and optional media type.
					case "request":
					case "response":
						//System.out.println("Line: "+line);
						//System.out.println("Remainder: "+remainder);
						if (remainder.indexOf('(') != -1) {
							String code = remainder.substring(0, remainder.indexOf('('));
							code.trim();
							node.setResponseCode(code);
						}
						else {
							String code = remainder.trim();
							if (code.length() > 0)
								node.setResponseCode(code);
						}
					
					//	These cases have a media type.
					case "model":
						if (remainder.indexOf('(') != -1) {						
							String media = remainder.substring(remainder.indexOf('(')+1, remainder.indexOf(')'));
							media.trim();
							node.setMedia(media);
						}
						node.setKeyword(kw);
						break;

					//	These cases just have an identifer.
					case "attributes":
					case "relation:":
						String id = remainder.trim();
						node.setIdentifier(id);
						node.setKeyword(kw);
						break;
						
					default:
						System.out.println("*** FAIL ***  Unknown list section pattern: "+line);
						break;
				}
			}
		}		
	}

	/**
	 * Pass 6:
	 * Collapse list sections.
	 * 
	 * The following list sections may have elements following them:
	 * <ul>
	 * <li>Schema - has an asset</li>
     * <li>Model - has a payload</li>
     * <li>Request - has a payload.</li>
     * <li>Response - has a payload.</li>
     * <li>Body - has a payload.</li>
     * <li>Parameters - has a list of parameters.</li>
     * <li>Attributes - has a list of attributes.</li>
     * <li>Headers - as a list of headers.</li>
	 * </ul>
	 * 
	 * @param nodeList
	 * @return  collapsed list.
	 */
	public List<BpNode> collapseLists_6(List<BpNode> nodeList) {
		List<BpNode> collapseList = new Vector<BpNode>();

		BpNode block = null;			//	List section we are collapsing into.
		boolean inBlock = false;			//	State flag set at start of a block.
		
		BpNode.Type blockName = BpNode.Type.UNKNOWN;
		BpNode.Type blockElement = BpNode.Type.UNKNOWN;

		//	Iterate over all nodes.
		for (BpNode node : nodeList) {
			BpNode.Type type = node.getType();
			String keyword = node.getKeyword();

			//	In a comment block?
			if (inBlock) {
				//	Check for an element to collapse.  Lump blank lines into a comment.
				if (type == BpNode.Type.UNKNOWN || type == BpNode.Type.BLANK) {
					
					//	We are supposed to have a list block at this point, if not, we have a problem.
					if (block == null) {
						MarkDownTest.valid = false;
						System.out.println("*** FAIL ***  Embedded collapsable element detected outside of a list section.");
					}
					
					//	Collapse the embedded comment element into the current description block.
					else {
						node.setType(blockElement);
						block.addChild(node);
						if (MarkDownTest.debug)
							System.out.println("    =========> Added an element to "+blockName+": "+node.getLine());
					}
				}
				
				//	Check to see if block is terminated by a header section.
				else if (type == BpNode.Type.HEAD) {
					inBlock = false;			//	Flag the end of an embedded block.
					block = null;			//	No longer have a block to collapse into.
					collapseList.add(node);		//	Add the list node.
					if (MarkDownTest.debug)
						System.out.println("    =========> Terminated by a "+node.getType()+" section: "+node.getLine());
				}
				
				//	Check to see if current block is terminated by a list section.
				else if (type == BpNode.Type.LIST) {
					//	See if the terminating list section is, itself, a collapsible section.
					if (this.collapseList(keyword)) {
						//	Switch on collapsible section keywords.
						switch (keyword) {
							case "schema":
							case "model":
							case "request":
							case "response":
							case "body":
								blockName = BpNode.Type.PAYLOAD_BLOCK;
								blockElement = BpNode.Type.PAYLOAD;
								break;
							case "parameters":
								blockName = BpNode.Type.PARAM_BLOCK;
								blockElement = BpNode.Type.PARAM;
								break;
							case "attributes":
								blockName = BpNode.Type.ATTRIB_BLOCK;
								blockElement = BpNode.Type.ATTRIB;
								break;
							case "headers":
								blockName = BpNode.Type.HEADER_BLOCK;
								blockElement = BpNode.Type.HEADER;
								break;
							default:
								//	Leave as UNKNOWN.
						}
						
						inBlock = true;								//	Flag the start of a collapsible block.
						block = new BpNode(blockName.toString());	//	Create a new block node.
						block.setType(blockName);				//	Set the description block type.
						node.addChild(block);					//	Add the new description block to the header node.
						collapseList.add(node);					//	Add the header to the output list.
						if (MarkDownTest.debug)
							System.out.println("=========> Re-starting a collapse block for: "+blockName+" in a "+keyword+" section.");						
					}
					else {
						inBlock = false;			//	Flag the end of an embedded block.
						block = null;				//	No longer have a block to collapse into.
						collapseList.add(node);		//	Add the list node.
						if (MarkDownTest.debug)
							System.out.println("    =========> Terminated by a "+node.getType()+" section: "+node.getLine());
					}
				}

				//	Commented terminated by something other than a list or header.
				else {
					inBlock = false;			//	Flag the end of an embedded block.
					block = null;			//	No longer have a block to collapse into.
					collapseList.add(node);		//	What ever it is, add it to the output list.

					MarkDownTest.valid = false;
					System.out.println("*** FAIL ***  Embedded description terminated by something other than head or list: "+type.toString());
				}
				

			}
			
			//	Not in a block, check for the start of a collapsible list.
			else if (type == BpNode.Type.LIST && this.collapseList(keyword)) {
				//	Switch on collapsible section keywords.
				switch (keyword) {
					case "schema":
					case "model":
					case "request":
					case "response":
					case "body":
						blockName = BpNode.Type.PAYLOAD_BLOCK;
						blockElement = BpNode.Type.PAYLOAD;
						break;
					case "parameters":
						blockName = BpNode.Type.PARAM_BLOCK;
						blockElement = BpNode.Type.PARAM;
						break;
					case "attributes":
						blockName = BpNode.Type.ATTRIB_BLOCK;
						blockElement = BpNode.Type.ATTRIB;
						break;
					case "headers":
						blockName = BpNode.Type.HEADER_BLOCK;
						blockElement = BpNode.Type.HEADER;
						break;
					default:
						//	Leave as UNKNOWN.
				}
				
				inBlock = true;								//	Flag the start of a collapsible block.
				block = new BpNode(blockName.toString());	//	Create a new block node.
				block.setType(blockName);				//	Set the description block type.
				node.addChild(block);					//	Add the new description block to the header node.
				collapseList.add(node);					//	Add the header to the output list.
				if (MarkDownTest.debug)
					System.out.println("=========> Starting a collapse block for: "+blockName+" in a "+keyword+" section.");
			}
			
			//	Not in a block, just copy the element.
			else {
				collapseList.add(node);		//	Could be a model, properties, or any number of things.
				if (MarkDownTest.debug)
					System.out.println("=========> Copy a section: "+node.getLine());
			}

		}
	
		return collapseList;
	}


	/****************************************************
	 *   Utility methods.                               *
	 ***************************************************/
	
	/**
	 * Return true if the key passed is a list section that may need to be collapsed.
	 * 
	 * @param key
	 * @return true if collapsible.
	 */
	boolean collapseList(String key) {
		if (key == null)
			return false;
		for (String cKey : MarkDownTest.COLLAPSE) {
			if (cKey.compareTo(key) == 0)
				return true;
		}
		return false;
	}
	
	/**
	 * Print the top level nodes to the console.
	 * 
	 * @param nodeList
	 */
	void flatPrint(List<BpNode> nodeList) {
	    for (BpNode n : nodeList) {
	    	if (n.getType() != BpNode.Type.BLANK)
	    		System.out.println(n.toString());
	    }
	}


	/**
	 * Print the BluePrint nodes as an indented tree structure.
	 * 
	 * WARNING!  This is a recursive method.
	 * 
	 * @param level
	 * @param node
	 */
	void treePrint(int level, BpNode node) {
		
		//	Output indentation.
		for (int i=0; i<level; i++)
			System.out.print("\t");
		
		//	If this is a blank line node, print an empty line.
    	if (node.getType() == BpNode.Type.BLANK)
    		System.out.println();
    	else
    		System.out.println(node.toString());
    	
    	//	Check for children and recurse if any are present.
    	List<BpNode> kids = node.getChildren();
    	if (kids != null && kids.size() > 0) {
    		int newLevel = level+1;
    		for (BpNode n : kids)
    			this.treePrint(newLevel, n);
    	}
	}

}
