package com.ms3_inc.api;

import java.util.List;
import java.util.Vector;

/**
 * A node in an API BluePrint document tree.
 * 
 * @author Mark J. Norton - mnorton@ms3-inc.com
 *
 */
public class BpNode {
	public static String[] KEYWORDS = {"model", "schema", "request", "response", "parameters", "attributes", "headers", "body", "data structure", "relation:"};
	public static String[] METHODS = {"GET", "PUT", "POST", "DELETE", "PATCH", "HEADER"};
	public enum Type {UNKNOWN, METADATA, HEAD, LIST, KEYWORD, BLANK, DESC_BLOCK, DESC, PAYLOAD_BLOCK, PAYLOAD, PARAM_BLOCK, PARAM, ATTRIB_BLOCK, ATTRIB, HEADER_BLOCK, HEADER};
	
	private Type type = Type.UNKNOWN;		//	Node type, see enum above.
	private String line = null;				//	A line of null indicates the root node.
	private List<BpNode>children = null;	//	Children, if any.
	
	private int indent = 0;					//	Count of initial spaces.
	private int level = 0;					//	Section level.
	
	private String keyword = null;			//	Keyword, if any.
	private String method = null;			//	Method, if any.
	private String uri = null;				//	A URI template.
	private String identifier = null;		//	An identifier.
	private String media = null;			//	A media type.
	private String responseCode = null;		//	HTTP response code.
	
	
	/**
	 * Constructor given a line of text.
	 * 
	 * A fair bit of parsing happens right here.  We capture indentation, sections + level, lists, and keywords.
	 * 
	 * @param line
	 */
	public BpNode(String line) {
		char[] chars = line.toCharArray();
		
		//	Check for a blank line.
		if (line.trim().length() == 0) {
			this.type = Type.BLANK;
			this.line = "";
		}
		
		else {
			//	Check for indentation.
			int off = 0;
			if (chars[0] == ' ') {
				while (chars[off] == ' ') {
					this.indent++;
					off++;
				}
			}
					
			//	Check for a header section.
			if (chars[off] == '#') {
				this.type = Type.HEAD;
				while (chars[off++] == '#')
					this.level++;			
			}
			
			//	Check for a list section.  
			//  In theory, lists can start with an asterisk, but this causes problems in some gold examples.
			//else if (chars[off] == '+' || chars[off] == '-' || chars[off] == '*') {
			else if (chars[off] == '+' || chars[off] == '-') {
				this.type = Type.LIST;
				off += 2;	//	Skip over the list symbol and space.
			}
			
			this.line = line.substring(off).trim();
		}
	}

	
	/**
	 * Keyword checks.
	 */
	public void keywords(char[] chars) {
		int len = this.line.length();
		int off = 0;
		
		//	check for a keyword.
		StringBuffer sb = new StringBuffer();
		
		//	Extract possible key word.
		int tempOff = off;
		while (chars[tempOff] != ' ' && chars[tempOff] != ':') {
			sb.append(chars[tempOff]);
			tempOff++;
			if (tempOff == len)
				break;
		}
		String candidate = sb.toString();
		
		//	Look up the key word.
		if (candidate.length() > 0) {
			//System.out.println ("-------------------- Keyword Candidate: ["+candidate+"]");
			for (String key : BpNode.KEYWORDS)
				if (candidate.compareTo(key) == 0)
					this.keyword = key;
		}
		if (this.keyword != null && this.type==Type.UNKNOWN)
			this.type = Type.KEYWORD;
		
		//	Check for a Method in a Section.
		if (candidate.length() > 0) {
			//System.out.println ("-------------------- Method Candidate: ["+candidate+"]");
			for (String key : BpNode.METHODS)
				if (candidate.compareTo(key) == 0)
					this.method = key;
		}		
		
		//	This might be a bit over general as there may be lines with a colon that are not metadata.
		if (this.type == Type.UNKNOWN && line.indexOf(":") != -1)
			this.type = Type.METADATA;
		
		this.line = line.substring(off).trim();

	}
	
	/**
	 * @return the line
	 */
	public String getLine() {
		return line;
	}

	/**
	 * @param line
	 */
	public void setLine(String line) {
		this.line = line;
	}

	/**
	 * @return the children
	 */
	public List<BpNode> getChildren() {
		return children;
	}

	/**
	 * @param children
	 */
	public void setChildren(List<BpNode> children) {
		this.children = children;
	}
	
	/**
	 * Add a child node.
	 * @param child
	 */
	public void addChild(BpNode child) {
		if (child == null)
			return;
		if (this.children == null)
			this.children = new Vector<BpNode>();
		this.children.add(child);
	}

	/**
	 * @return the indent
	 */
	public int getIndent() {
		return indent;
	}

	/**
	 * @param indent the indent to set
	 */
	public void setIndent(int indent) {
		this.indent = indent;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	/**
	 * @return the keyword
	 */
	public String getKeyword() {
		return keyword;
	}

	/**
	 * @param keyword the keyword to set
	 */
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @return the uri
	 */
	public String getUri() {
		return uri;
	}


	/**
	 * @param uri the uri to set
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}


	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}


	/**
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}


	/**
	 * @return the media
	 */
	public String getMedia() {
		return media;
	}


	/**
	 * @param media the media to set
	 */
	public void setMedia(String media) {
		this.media = media;
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}


	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	
	/***********************************
	 * Utility Methods                 *
	 **********************************/
	
	/**
	 * Scan the line for any of the METHOD keywords.  If found, the METHOD string is returned,
	 * otherwise null.
	 * 
	 * NOTE:  Relies on methods being upper case.
	 * 
	 * @return  METHOD or null.
	 */
	public String findMethod() {
		
		for (String method : BpNode.METHODS) {
			if (this.line.indexOf(method) != -1)
				return method;
		}
		
		return null;
	}
	
	/**
	 * Scan the line for any of the list KEYWORDS.  If found the KEYWORD string is returned,
	 * otherwise null.
	 * 
	 * @return KEYWORD or null.
	 */
	public String findKeyword () {
		String lowerLine = this.line.toLowerCase();		//  Handles insensitive case.

		for (String key : BpNode.KEYWORDS) {
			if (lowerLine.indexOf(key) != -1)
				return key;
		}
		return null;
	}
	
	/**
	 * Renders the node for diagnostic purposes.  The current string patterns are:
	 * 
	 * [HEAD.LEVEL][KEYWORD] line
	 * [LIST.INDENT][KEYWORD] line
	 * [TYPE] line
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		//	Type info.
		sb.append("["+this.type);
		if (this.type == Type.HEAD)
			sb.append("."+this.level);
		else if (this.type == Type.LIST)
			sb.append("."+this.indent);
		sb.append("]");
		
		//	Add keyword if this is a header or list.
		String kw = this.keyword;
		if (kw == null) kw = "UNKNOWN";
		if (this.type == Type.HEAD || this.type == Type.LIST) {
			sb.append("["+kw+"]");
		}
						
		//	Add the line.
		sb.append(" "+this.line);
		
		return sb.toString();
	}

}
