package com.ms3_inc.api;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.raml.v2.api.RamlModelBuilder;
import org.raml.v2.api.RamlModelResult;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import com.wordnik.swagger.models.Swagger;
import com.wordnik.swagger.util.Json;
import com.wordnik.swagger.util.Yaml;

import io.swagger.parser.SwaggerParser;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class ApiValidate {

	private static final String SEPARATOR = "/";
	private static final String API_RAML_NAME = "api.raml";
	private static final String API_SWAGGER_YAML_NAME = "api.yaml";
	private static final String API_SWAGGER_JSON_NAME = "api.jsom";
	
	private static final SwaggerParser SWAGGER_PARSER = new SwaggerParser();
	private static final RamlModelBuilder RAML_PARSER = new RamlModelBuilder();
	
	/**
	 * A content stub.
	 * 
	 * @return stubbed string.
	 */
	public String doContent() {
		String ret = "Validation results go here.<br>";
		return ret;
	}

	

	/**
	 * Validate the file given using the type passed.
	 * 
	 * @param f
	 * @param type
	 * 
	 * @return validation results.
	 */
	public String validate(File mainApiFile, String fn, String type) {

		String ret = "Unknow protocol type.";

		// RAML 0.8 Validation.
		if (type.compareTo("raml08") == 0) {
			if (validateRaml08(mainApiFile))
				ret = "valid API [" + type + "]";
			else
				ret = "invalid API [" + type + "]";

		}

		// RAML 1.0 Validation.
		else if (type.compareTo("raml10") == 0) {
			if (validateRaml10(mainApiFile))
				ret = "valid API [" + type + "]";
			else
				ret = "invalid API [" + type + "]";
		}

		// Swagger Validation.
		else if (type.compareTo("swagger") == 0) {
			if (validateSwagger(mainApiFile))
				ret = "valid API [" + type + " | " + fn + "]";
			else
				ret = "invalid API [" + type + " | " + fn + "]";
		}

		// BluePrint Validation.
		else if (type.compareTo("blueprint") == 0) {
			if (validateBluePrint(mainApiFile))
				ret = "valid API [" + type + "]";
			else
				ret = "invalid API [" + type + "]";
		}

		return ret;
	}

	public boolean validateBluePrint(File mainApiFile) {
		// String apiStr = "work in progress";
		return false;
	}

	public boolean validateRaml08(File mainApiFile) {		
		boolean valid = false;
		File tmpFolder = null;
		File myApi = null;
		try {
			if (isZippedFile(mainApiFile)) {
				tmpFolder = extractToTmpFolder(mainApiFile);
				myApi = new File(tmpFolder.getAbsolutePath() + SEPARATOR + API_RAML_NAME);
			} else
				myApi = mainApiFile;
			
			RamlModelResult ramlModelResult = RAML_PARSER.buildApi(myApi);
			org.raml.v2.api.model.v08.api.Api ramlApi = ramlModelResult.getApiV08();
			if (ramlApi != null)
				valid = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (isZippedFile(mainApiFile) && tmpFolder != null)
				deleteTmpFolder(tmpFolder);
		}
		return valid;
	}

	


	public boolean validateRaml10(File mainApiFile) {
		
		boolean valid = false;
		File tmpFolder = null;
		File myApi = null;
		try {
			
			if (isZippedFile(mainApiFile)) {
				tmpFolder = extractToTmpFolder(mainApiFile);
				myApi = new File(tmpFolder.getAbsolutePath() + SEPARATOR + API_RAML_NAME);
			} else
				myApi = mainApiFile;

			RamlModelResult ramlModelResult = RAML_PARSER.buildApi(myApi);
			org.raml.v2.api.model.v10.api.Api ramlApi = ramlModelResult.getApiV10();
			if (ramlApi != null)
				valid = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (isZippedFile(mainApiFile) && tmpFolder != null)
				deleteTmpFolder(tmpFolder);
		}
		return valid;
	}

	public boolean validateSwagger(File mainApiFile) {
		
		File tmpFolder = null;
		File myApi = null;
		boolean valid = false;				
		ObjectMapper mapper = null;
		
		try {
			
			if (isZippedFile(mainApiFile)) {
				tmpFolder = extractToTmpFolder(mainApiFile);				
				myApi = new File(tmpFolder.getAbsolutePath() + SEPARATOR + API_SWAGGER_YAML_NAME);
				if (!myApi.exists()){
					myApi = new File(tmpFolder.getAbsolutePath() + SEPARATOR + API_SWAGGER_JSON_NAME);
					if (!myApi.exists())
						return false;
				}
			} else
				myApi = mainApiFile;
			
			if (isYaml(myApi))
				mapper = Yaml.mapper();
			else
				mapper = Json.mapper();

			JsonNode root = mapper.readTree(myApi);
			Swagger ramlApi = SWAGGER_PARSER.read(root);

			if (ramlApi != null)
				valid = true;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (isZippedFile(mainApiFile) && tmpFolder != null)
				deleteTmpFolder(tmpFolder);
		}
		return valid;
	}
	
	private void deleteTmpFolder(File tmpFolder) {
		try {
			FileUtils.deleteDirectory(tmpFolder);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("TMP Folder deleted");
	}

	private File extractToTmpFolder(File mainApiFile) throws ZipException {
		File tmpFolder;
		ZipFile myZippedfile = new ZipFile(mainApiFile);
		tmpFolder = Files.createTempDir();
		myZippedfile.extractAll(tmpFolder.getAbsolutePath());
		System.out.println("Created tmp Folder:" + tmpFolder.getAbsolutePath());
		return tmpFolder;
	}
	
	private boolean isZippedFile(File mainApiFile) {
		if (mainApiFile.getAbsolutePath().toLowerCase().indexOf(".zip") > -1)
			return true;
		else
			return false;
	}
	
	/**
	 * IF IS NOT YAML ---> IS JSON
	 * @param fileName
	 * @return
	 */
	private boolean isYaml(File fileName) {
		if (fileName.getAbsolutePath().toLowerCase().indexOf(".yaml") > -1)
			return true;
		else
			return false;
	}

	
}
