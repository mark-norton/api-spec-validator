package com.ms3_inc.api;

import java.io.File;

public class ValidateAPICommand {

	private static final String WORK_IN_PROGRESS = "Work in progress";
	private static final String INFO = "Two arguments are mandatory\nArgument#1 (RAML08|RAML10|SWAGGER|BP) \nArgument#2 <absolutePathAPI File> Plain Text API and ZIP format are supported";
	private static final String MSG_INVALID_MODE = "invalid Argument#1 options are (RAML08|RAML10|SWAGGER|BP)";
	private static final String MSG_INVALID_FILE = "invalid Argument#2 file must exist in local fileSystem.Plain Text API and ZIP format are supported. ";
	

	

	public static void main(String[] args) {
		
//		args = new String[2];		
//		args[0]="RAML08";
//		args[1]="C:/dev/workspace-ms3/api-spec-validator/resources/valid/zip/raml08.zip";
		
		if (args.length == 2) {
			String mode = args[0];
			String filePath = args[1];
			File myApiFile = new File(filePath);
			if (!myApiFile.exists()) {
				System.err.println(MSG_INVALID_FILE);
				exit();
			}
			ApiValidate apiValidator = new ApiValidate();
			switch (mode) {
			case "RAML08":
				printValidationResult(apiValidator.validateRaml08(myApiFile),mode);
				break;
			case "RAML10":
				printValidationResult(apiValidator.validateRaml10(myApiFile),mode);
				break;
			case "SWAGGER":
				printValidationResult(apiValidator.validateSwagger(myApiFile),mode);
				break;
			case "BP":
				System.err.println(WORK_IN_PROGRESS);
				exit();
				break;

			default:
				System.err.println(MSG_INVALID_MODE);
				exit();
				break;
			}

		} else {
			System.err.println(INFO);
			exit();
		}

	}

	private static void printValidationResult(boolean validateRaml08, String mode) {
		if (validateRaml08)
			System.out.println("[VALID] "+mode+" API Definition.");
		else
			System.out.println("[INVALID] "+mode+" API Definition.");
		
	}

	private static void exit() {		
		System.exit(1);
	}

}
