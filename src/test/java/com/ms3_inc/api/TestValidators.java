package com.ms3_inc.api;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import net.lingala.zip4j.exception.ZipException;

public class TestValidators {
	
	
	private static final String RAML_08_VALID = "resources/valid/api_raml_08.raml";
	private static final String RAML_08_INVALID = "resources/invalid/api_raml_08.raml";
	private static final String RAML_10_VALID = "resources/valid/api_raml_10.raml";
	private static final String RAML_10_INVALID = "resources/invalid/api_raml_10.raml";
	
	private static final String RAML_08_COMPLEX_ZIP_VALID = "resources/valid/zip/raml08.zip";
	private static final String RAML_08_COMPLEX_ZIP_INVALID = "resources/invalid/zip/raml08.zip";
	private static final String RAML_10_COMPLEX_ZIP_VALID = "resources/valid/zip/raml10.zip";
	private static final String RAML_10_COMPLEX_ZIP_INVALID = "resources/invalid/zip/raml10.zip";
	
	private static final String SWAGGER_JSON_VALID = "resources/valid/api_swagger_json.json";
	private static final String SWAGGER_JSON_INVALID = "resources/invalid/api_swagger_json.json";
	private static final String SWAGGER_YAML_VALID = "resources/valid/api_swagger_yaml.yaml";	
	private static final String SWAGGER_YAML_COMPLEX_ZIP_VALID = "resources/valid/zip/swagger_yaml.zip";
	private static final String SWAGGER_YAML_COMPLEX_ZIP_INVALID = "resources/invalid/zip/swagger_yaml.zip";
	
	static ApiValidate ApiValidator = null;


	@Before
	public void init() {
		ApiValidator = new ApiValidate();
	}

/**
*				VALID TESTS	
 * @throws IOException 
 * @throws ZipException 
*/	
	
	@Test
	public void raml08SimpleRaml_Valid() throws FileNotFoundException {
		assertTrue(ApiValidator.validateRaml08(new File(RAML_08_VALID)));		
	}
	
	@Test
	public void raml08ComplexZipRaml_Valid() throws FileNotFoundException {		
		assertTrue(ApiValidator.validateRaml08(new File(RAML_08_COMPLEX_ZIP_VALID)));
	}	

	@Test
	public void raml10SimpleRaml_Valid() throws FileNotFoundException {
		assertTrue(ApiValidator.validateRaml10(new File(RAML_10_VALID)));
	}
	
	@Test
	public void raml10ComplexZipRaml_Valid() throws FileNotFoundException {		
		assertTrue(ApiValidator.validateRaml10(new File(RAML_10_COMPLEX_ZIP_VALID)));
	}


	@Test
	public void SwaggerSimpleValidJson() throws FileNotFoundException {
		assertTrue(ApiValidator.validateSwagger(new File(SWAGGER_JSON_VALID)));
	}
	
	@Test
	public void SwaggerSimpleValidYaml() throws FileNotFoundException {
		assertTrue(ApiValidator.validateSwagger(new File(SWAGGER_YAML_VALID)));
	}
	
	@Test
	public void SwagerComplexZipYaml_Valid() throws FileNotFoundException {
		assertTrue(ApiValidator.validateSwagger(new File(SWAGGER_YAML_COMPLEX_ZIP_VALID)));
	}

///**
//*				INVALID TESTS	
//*/	
	
	@Test
	public void raml08ComplexZipRaml_Invalid() throws FileNotFoundException {		
		assertFalse(ApiValidator.validateRaml08(new File(RAML_08_COMPLEX_ZIP_INVALID)));
	}
	
	@Test
	public void raml10ComplexZipRaml_Invalid() throws FileNotFoundException {		
		assertFalse(ApiValidator.validateRaml10(new File(RAML_10_COMPLEX_ZIP_INVALID)));
	}
	
	@Test
	public void raml08SimpleInvalidRaml() throws FileNotFoundException {
		assertFalse(ApiValidator.validateRaml08(new File(RAML_08_INVALID)));
	}

	@Test
	public void raml10SimpleInvalidRaml() throws FileNotFoundException {
		assertFalse(ApiValidator.validateRaml10(new File(RAML_10_INVALID)));
	}
	

	@Test
	public void SwaggerSimpleInvalidRaml() throws FileNotFoundException {
		assertFalse(ApiValidator.validateSwagger(new File(SWAGGER_JSON_INVALID)));
	}
	
	@Test
	public void SwagerComplexZipYaml_invalid() throws FileNotFoundException {
		assertFalse(ApiValidator.validateSwagger(new File(SWAGGER_YAML_COMPLEX_ZIP_INVALID)));
	}

}
