# README #

The API Specification Validator application is a web-based app that allows API specs in RAML, Swagger, or BluePrint formats to be uploaded and validated against current parsers.  The result is an UP/DOWN result with the exception messages that caused the failure.

### What is this repository for? ###

* Validate RAML, Swagger, and BluePrint files.
* 1.0.0 - target release.

### How do I get set up? ###

* Clone the source.
* Building using "mvn clean package"
* Dependencies
** Requires Parser libraries.
** Runs in a Tomcat environment.
* How to run tests - TBD
* Deployment instructions
** Deploy as a WAR to Tomcat.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Mark J. Norton - mnorton@ms3-inc.com.
