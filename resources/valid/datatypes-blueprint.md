FORMAT: 1A
HOST: http://ms3-inc.com

# dataval_datatypes

#### GET /part/all

+ Parameters

    + maxResults
    
    + code: P05,P10,P15,P20,SPEC
    
    + show
    
    + commission
    
    + intro

+ Request

    + Headers

            Accept: application/json

    + Body

+ Response 200 (application/json)

    + Body

### /part/instock/{stockFlag}

+ Parameters

    + stockFlag (required)

#### GET

+ Request

    + Headers

            Accept: application/json

    + Body

+ Response 200 (application/json)

    + Body

### /part/added/{addDate}

+ Parameters

    + addDate (required)

#### GET

+ Request

    + Headers

            Accept: application/json

    + Body

+ Response 200 (application/json)

    + Body

### /part/price/{limit}

+ Parameters

    + limit (required)

#### GET

+ Request

    + Headers

            Accept: application/json

    + Body

+ Response 200 (application/json)

    + Body

### /part/name/{spec}

+ Parameters

    + spec (required)

#### GET

+ Request

    + Headers

            Accept: application/json

    + Body

+ Response 200 (application/json)

    + Body

### /part/id/{id}

+ Parameters

    + id (required)

#### GET

+ Request

    + Headers

            Accept: application/json

    + Body

+ Response 200 (application/json)

    + Body

